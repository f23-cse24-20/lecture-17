#include <iostream>
using namespace std;

void printGreeting(string name) {
    cout << "Hello, " << name << "." << endl;
}

int main() {

    /*
        Create a function that takes in a name and outputs a personalized greeting.

        Example:
        Hello, Alex.
    */

    string name;
    getline(cin, name);

    printGreeting(name);


    return 0;
}

