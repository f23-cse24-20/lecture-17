#include <iostream>
using namespace std;

void printBanner(string text) {
    for (int i = 0; i < text.length() + 2; i++) {
        cout << "=";
    }
    cout << endl;

    cout << " " << text << " " << endl;

    for (int i = 0; i < text.length() + 2; i++) {
        cout << "=";
    }
    cout << endl;
}

void displayBanner(string heading) {
    system("clear");
    printBanner(heading);
    cout << endl;
}

int main() {

    /*
        Create a function that takes in a heading and prints a banner.
        Create program which repeatedly asks user to input a heading and prints out the heading.

        Example:
        ===========
         UC Merced 
        ===========
    */

    string heading;

    while(getline(cin, heading)) {
        displayBanner(heading);
    }

    return 0;
}