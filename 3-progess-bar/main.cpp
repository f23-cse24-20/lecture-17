#include <iostream>
using namespace std;

void printProgressBar(int percentage) {
    if (percentage < 0) {
        percentage = 0;
    }
    if (percentage > 100) {
        percentage = 100;
    }
    
    int ticks = 20;

    int complete = (percentage / 100.0) * ticks;

    cout << "[";
    for (int i = 0; i < complete; i++) {
        cout << "=";
    }
    for (int i = complete; i < ticks; i++) {
        cout << " ";
    }
    cout << "] | " << percentage << "%" << endl;
}

void displayProgressBar(int percentage) {
    system("clear");
    printProgressBar(percentage);
    cout << endl;
}

int main() {

    /*
        Create a function that takes in a percentage and prints a progress bar.
        Create program which repeatedly asks user to input a percentage and prints out a progress bar.

        Example:
        [===============     ] | 75%
    */

    int percentage;

    while (cin >> percentage) {
        displayProgressBar(percentage);
    }

    

    return 0;
}